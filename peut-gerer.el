;;; peut-gerer -- Peut-gerer Enables Users To...manage project workflows

;; Copyright (C) 2020 Matt Trzcinski (matt AT excalamus DOT com)

;; This file is not part of GNU Emacs.

;; Author: Matt Trzcinski
;; Version: 1.0.0
;; Package-Requires: ((emacs "26.3"))
;; Keywords:  project, management, shell
;; URL: https://codeberg.org/excalamus/peut-gerer

;; This is free software; you can redistribute it and/or modify it under
;; the terms of the GNU General Public License as published by the Free
;; Software Foundation; either version 3, or (at your option) any later
;; version.
;;
;; This is distributed in the hope that it will be useful, but WITHOUT
;; ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
;; FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
;; for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs; see the file COPYING.  If not, write to the
;; Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
;; MA 02110-1301 USA.

;;; Code:


;;; Requirements:
(require 'cl-lib)
(require 'comint)


;;; Variables:

(defvar peut-gerer-project-alist nil
  "Project definitions.

Define projects using nested alists.

Valid keys:

shells              alist of shell-name, root, and setup commands
  root              path to shell's root
  setup             list of shell/lisp commands to run when initializing shell
paths               list of paths corresponding to which files to load.
                    Paths have three forms:
                    1. File – load specific file.  Use absolute paths for best
                       results.  E.g. `/load/this/exact/file.c'
                    2. Directory – load all files at the directory level. **Not**
                       recursive!  You must explicitly list all the directories
                       to load.  E.g. `/load/files/in/directory/top/level/'
                    3. Wildcard – glob directory for type of file.
                       E.g. `/load/only/C/files/*.c'
commands            list of shell commands
prefix              string used to construct commands
window-split        how to arrange Emacs's windows.  Use `:horizontal',
                    `:vertical', or `:quad'.  Define custom splits with
                    `peut-gerer-window-split-alist'
hooks               alist of functions to run before or after activation and
                    deactivation.  Hook functions take no arguments.
  before-activate   List of functions to call before activating a project.
                    No project files or processes available.
  after-activate    List of functions to call after activating a project.
                    All project files and processes available.
  before-deactivate List of functions to call before deactivating a project.
                    All project files and processes available.
  after-deactivate  List of functions to call after deactivating a project.
                    No project files or processes.

Example:

    (setq peut-gerer-project-alist
        '((my_c_project .
                        ((shells .
                                 ((shell-name-1 .
                                                ((root . \"/path/to/build/directory/\")
                                                (setup . (\"export MYBUILDVAR1=1\"
                                                          \"export MYBUILDVAR2=0\"))))
                                    (shell-name-2 .
                                                ((root . \"/path/to/run/directory/\")
                                                (setup . (\"export MYRUNVAR=1\"))))))
                        (paths . (\"/path/to/entrypoint.c\"
                                    \"/path/to/other/file/I/care/about.c\"))
                        (commands . (\"./build_my_project.sh\"
                                        \"./run_my_build\"))
                        (window-split . :quad)))
            (my-python-project .
                            ((shells .
                                        ((my-python-project .
                                                            ((root . \"/path/to/my/python/project\")
                                                            (setup . (\"source venv/bin/activate\"))))))
                                (paths . (\"/path/to/my_python_project/entry_point.py\"))
                                (commands . (\"python3 -m my_python_project\"
                                             \"python3 -m unittest discover tests/ --failfast --quiet\"))
                                (prefix . \"python3 \")
                                (window-split . :vertical)
                                (hooks . ((before-activate . ((lambda () (message \"before-activate1\"))
                                                              (lambda () (message \"before-activate2\"))))
                                          (after-activate . ((lambda () (message \"after-activate\"))))
                                          (before-deactivate . ((lambda () (message \"before-deactivate\"))))
                                          (after-deactivate . ((lambda () (message \"after-deactivate\"))))))))))")

(defvar peut-gerer-project nil
  "Name of the active project.")

(defvar peut-gerer-current-shell nil
  "Primary shell process buffer.")

(defvar peut-gerer-command ""
  "Primary shell command.

This is a string which is sent to `peut-gerer-current-shell' by
`peut-gerer-send-command'.  Use `peut-gerer-set-command' to set
interactively.")

(defvar peut-gerer-command-prefix nil
  "Prefix to use in shell calls.

This is typically an executable such as \"python \".")

(defvar peut-gerer-window-split-alist
  '((:quad .
           (lambda (&optional args)
             (let* ((alist (alist-get peut-gerer-project (car args)))
                    (file1 (if peut-gerer-files (nth 0 peut-gerer-files)))
                    (file2 (if peut-gerer-files (nth 1 peut-gerer-files)))
                    (file3 (if peut-gerer-files (nth 2 peut-gerer-files)))
                    (file4 (if peut-gerer-files (nth 3 peut-gerer-files))))
               (delete-other-windows)
               (if file1 (find-file file1))
               (split-window-horizontally)
               (other-window 1)
               (if file3 (find-file file3))
               (split-window-vertically)
               (other-window 2)
               (split-window-vertically)
               (other-window 1)
               (if file2 (find-file file2))
               (other-window 2)
               (cond (peut-gerer-shell-names
                      (switch-to-buffer (car peut-gerer-shell-names)))
                     (file4 (find-file file4))))))
    (:vertical .
               (lambda (&optional args)
                 (let* ((alist (alist-get peut-gerer-project (car args)))
                        (file1 (if peut-gerer-files (nth 0 peut-gerer-files)))
                        (file2 (if peut-gerer-files (nth 1 peut-gerer-files))))
                   (delete-other-windows)
                   (if file1 (find-file file1))
                   (split-window-horizontally)
                   (cond (peut-gerer-shell-names
                          (other-window 1)
                          (switch-to-buffer (car peut-gerer-shell-names)))
                         (file2 (find-file file2))))))
    (:horizontal .
                 (lambda (&optional args)
                   (let* ((alist (alist-get peut-gerer-project (car args)))
                          (file1 (if peut-gerer-files (nth 0 peut-gerer-files)))
                          (file2 (if peut-gerer-files (nth 1 peut-gerer-files))))
                     (delete-other-windows)
                     (if file1 (find-file file1))
                     (split-window-vertically)
                     (other-window 1)
                     (cond (peut-gerer-shell-names
                            (switch-to-buffer (car peut-gerer-shell-names)))
                           (file2 (find-file file2)))))))
  "Window split configurations.

Keys are configurations and values are functions which take an
optional alist, such as `peut-gerer-project-alist'.  See
`peut-gerer-split-frame'.")

(defvar peut-gerer-files nil
  "List of files corresponding to project.")

(defvar peut-gerer-shell-names nil
  "List of shell buffer names corresponding to project.")

(defvar peut-gerer-shell-procs nil
  "List of shell processes corresponding to project.")

(defvar peut-gerer-roots nil
  "List of roots corresponding to project shells.")

(defvar peut-gerer--before-activate-hooks nil
  "List of functions to be called before activating a project.
No files or processes will be listed (opened or live) before the
functions are called.")

(defvar peut-gerer--after-activate-hooks nil
  "List of functions to be called after activating a project.
All files and processes will be listed (opened or live) before
the functions are called.")

(defvar peut-gerer--before-deactivate-hooks nil
  "List of functions to be called before deactivating a project.
All files and processes will be listed (opened or live) before
the functions are called.")

(defvar peut-gerer--after-deactivate-hooks nil
  "List of functions to be called after deactivating a project.
No files or processes will be listed (opened or live) before the
functions are called.")

(defvar peut-gerer--command-history nil
  "Command history for `peut-gerer-send-command'.

When more than one command is defined for a project, the cdr of
the commands list is appended to the history.")


;;; Functions:

(defun peut-gerer-kill-all-visiting-buffers (dir)
  "Kill all buffers visiting DIR."
  (interactive "DKill all buffers visiting directory: ")
  (remq nil
        (mapcar
         (lambda (buf)
           (let ((bfn (buffer-file-name buf)))
             (cond ((string-prefix-p (file-truename dir) bfn)
                    (kill-buffer buf)
                    bfn))))
         (buffer-list))))

(defun peut-gerer-deactivate (project &optional prompt)
  "Clean up PROJECT environment.

If PROMPT, ask whether to proceed with deactivation.

Deactivating a project kills all buffers corresponding to the
`paths' keyword, all buffers corresponding to files containing a
`root', as well as all processes created during activation.
Files are saved before being closed."
  (interactive
   (list peut-gerer-project t))
  (if (not peut-gerer-project)
      (error "No active project"))
  (catch 'canceled
    (if (and prompt
             (not (y-or-n-p (format "Deactivate project '%s'" peut-gerer-project))))
        (throw 'canceled "Project deactivation canceled by user"))
    ;; Proceed with deactivation
    (let-alist (assoc peut-gerer-project peut-gerer-project-alist)
      (let* ((kill-buffer-query-functions nil))

        (run-hooks 'peut-gerer--before-deactivate-hooks)

        ;; save all buffers without question
        (save-some-buffers t)

        ;; kill buffers visiting peut-gerer-files
        (mapc #'(lambda (buf)
                  (let ((bfn (buffer-file-name buf)))
                    (cond ((member bfn peut-gerer-files)
                           (kill-buffer buf)
                           bfn))))
              (buffer-list))

        ;; kill buffers visiting files in roots
        (cl-loop for root in peut-gerer-roots
                 do
                 (peut-gerer-kill-all-visiting-buffers root))

        ;; kill processes
        (cl-loop for shell-name in peut-gerer-shell-names
                 do
                 (let ((proc (get-buffer-process shell-name)))
                   (if proc ; user may have killed it already
                       (kill-buffer (process-buffer proc)))))  ; process buffers are not regular buffers

        ;; reset state
        (setq peut-gerer-project nil)
        (setq peut-gerer-files nil)
        (setq peut-gerer-shell-names nil)
        (setq peut-gerer-shell-procs nil)
        (setq peut-gerer-roots nil)

        (run-hooks 'peut-gerer--after-deactivate-hooks)

        ;; reset hooks
        (setq peut-gerer--before-activate-hooks nil)
        (setq peut-gerer--after-activate-hooks nil)
        (setq peut-gerer--before-deactivate-hooks nil)
        (setq peut-gerer--after-deactivate-hooks nil)

        (message "Deactivated project: '%s'" project)))))

(defun peut-gerer-create-shell (name)
  "Create shell with a given NAME.

NAME should have earmuffs (e.g. *NAME*) if it is to follow Emacs
naming conventions.  Earmuffs indicate that the buffer is special
use and not associated with a file.

Returns newly created shell process.

Adapted from URL `https://stackoverflow.com/a/36450889/5065796'"
  (interactive
   (let ((name (read-string "Create shell name: " nil)))
     (list name)))
  (let ((name (or name peut-gerer-current-shell)))
    (get-buffer-process (shell name))))

(defun peut-gerer-send-command (command &optional pbuff beg end)
  "Send COMMAND to shell process with buffer name PBUFF.

PBUFF is the buffer name string of a process.  If the process
associated with PBUFF does not exist, it is created.  PBUFF is
then opened in the other window and control is returned to the
calling buffer.

See URL `https://stackoverflow.com/a/7053298/5065796'"
  (interactive
   (let* ((prompt (format "Send to %s: " peut-gerer-current-shell))
          (cmd (read-string prompt "" 'peut-gerer--command-history peut-gerer-command)))
     (list cmd peut-gerer-current-shell)))
  (let* ((pbuff (or pbuff peut-gerer-current-shell))
         (proc (or (get-buffer-process pbuff)
                   ;; create new process
                   (let ((currbuff (current-buffer))
                         (new-proc (peut-gerer-create-shell pbuff)))  ; creates a buried pbuff
                     (switch-to-buffer-other-window pbuff)
                     (switch-to-buffer currbuff)
                     new-proc)))
         (command-and-go (concat command "\n")))
    (with-current-buffer pbuff
      (goto-char (process-mark proc))
      (insert command-and-go)
      (move-marker (process-mark proc) (point)))
    (process-send-string proc command-and-go)
    (with-current-buffer pbuff
      (comint-add-to-input-history command))))

(defun peut-gerer-split-frame (split &rest args)
  "Divide frame according to SPLIT.

SPLIT corresponds to keys of `peut-gerer-window-split-alist'.
The optional ARGS is an alist such as
`peut-gerer-project-alist'."
  (interactive
   (list (intern
          (completing-read
           "Split window: "
           (mapcar 'car peut-gerer-window-split-alist) nil t))))
  (funcall
   (alist-get split peut-gerer-window-split-alist) args))

(defun peut-gerer-activate (project)
  "Set up PROJECT environment.

PROJECT is a symbol corresponding to a top-level key of
`peut-gerer-project-alist'."
  (interactive
   (let* ((deactivate-p
           (if peut-gerer-project
               (y-or-n-p (format "Project '%s' currently active. Deactivate?" peut-gerer-project)))))
     (cond ((and peut-gerer-project deactivate-p)
            (peut-gerer-deactivate peut-gerer-project))
           ((and peut-gerer-project (not deactivate-p))
            (error (format "Activation aborted. Project '%s' still active" peut-gerer-project))))
     (list (intern
            (completing-read
             "Activate project: "
             (mapcar 'car peut-gerer-project-alist) nil t)))))
  (catch 'abort
    (condition-case err
        (cl-labels ((flatten (x acc)  ; onlisp p.44  WARNING: recursive
                             (cond ((null x) acc)
                                   ((atom x) (cons x acc))
                                   (t (flatten (car x) (flatten (cdr x) acc))))))
          (setq peut-gerer-project project)
          (let-alist (assoc project peut-gerer-project-alist)
            (let* ((procs)  ; create after root and paths validated
                   (shells (mapcar 'car .shells))
                   (shell-names (mapcar #'(lambda (sh) (concat "*" (symbol-name sh) "*")) shells))
                   (roots (mapcar #'(lambda (sh)
                                      (let ((root (alist-get 'root sh)))
                                        (if (file-exists-p root)
                                            root
                                          (error (format "Invalid root '%s'" root)))))
                                  .shells))
                   (setup-cmds (mapcar #'(lambda (sh) (alist-get 'setup sh)) .shells))
                   (files (flatten (cl-mapcar
                                    #'(lambda (path)
                                        (cond ((file-regular-p path)  ; file
                                               path)
                                              ((file-directory-p path)  ; directory
                                               (directory-files path t directory-files-no-dot-files-regexp))
                                              ((file-expand-wildcards path))  ; wildcard
                                              (t (error (format "Invalid path '%s'" path)))))
                                    .paths)
                                   nil)))
              ;; add hooks
              (mapc
               #'(lambda (fn) (add-hook 'peut-gerer--before-activate-hooks fn 100))
               .hooks.before-activate)

              (mapc
               #'(lambda (fn) (add-hook 'peut-gerer--after-activate-hooks fn 100))
               .hooks.after-activate)

              (mapc
               #'(lambda (fn) (add-hook 'peut-gerer--before-deactivate-hooks fn 100))
               .hooks.before-deactivate)

              (mapc
               #'(lambda (fn) (add-hook 'peut-gerer--after-deactivate-hooks fn 100))
               .hooks.after-deactivate)

              (run-hooks 'peut-gerer--before-activate-hooks)

              ;; back up
              (setq peut-gerer-files files)
              (setq peut-gerer-shell-names shell-names)
              (setq peut-gerer-roots roots)

              (setq procs (mapcar 'peut-gerer-create-shell shell-names))
              (setq peut-gerer-shell-procs procs)

              ;;;;;;;;;;;;
              ;; shells ;;
              ;;;;;;;;;;;;
              ;; change current directory to root
              (cl-mapc
               '(lambda (root shell-name)
                  (peut-gerer-send-command (concat "cd " root) shell-name))
               roots
               shell-names)

              ;;; run setup commands
              (cl-mapc
               '(lambda (shell-name cmds)
                  (cl-loop for cmd in cmds
                           do
                           (peut-gerer-send-command cmd shell-name)))
               shell-names
               setup-cmds)

              ;;; set current
              (setq peut-gerer-current-shell (car shell-names))

              ;;;;;;;;;;;;;;
              ;; commands ;;
              ;;;;;;;;;;;;;;
              (setq peut-gerer-command-prefix .prefix)

              (cond ((and .prefix files (not .commands))
                     (setq peut-gerer-command (concat .prefix (car files))))
                    (.commands
                     (setq peut-gerer-command (car .commands))
                     (setq peut-gerer--command-history nil)
                     (mapc #'(lambda (cmd)
                               (add-to-history 'peut-gerer--command-history cmd))
                           (cdr .commands))))

              ;;;;;;;;;;;
              ;; files ;;
              ;;;;;;;;;;;
              ;; load files
              (mapc #'(lambda (f) (find-file f t)) files)

              ;;;;;;;;;;;
              ;; frame ;;
              ;;;;;;;;;;;
              (cond (.window-split
                     (peut-gerer-split-frame .window-split (car peut-gerer-project-alist)))
                    (files
                     (find-file (car files) t)))

              (run-hooks 'peut-gerer--after-activate-hooks)

              (message "Activated '%s' project" project))))
          (error
           (progn
             (message "Error activating '%s'. Aborting" peut-gerer-project)
             (peut-gerer-deactivate peut-gerer-project)
             (message "%s" (error-message-string err))
             (throw 'abort nil))))))

(defun peut-gerer-set-shell (pbuff)
  "Set `peut-gerer-current-shell' to the buffer name PBUFF associated with a process.

For example, if 'shell' is a process, PBUFF is the buffer name
\"*shell*\" associated with it."
  (interactive
   (list (read-buffer "Set shell to: " nil t '(lambda (x) (processp (get-buffer-process (car x)))))))
  (setq peut-gerer-current-shell pbuff)
  (message "Set `peut-gerer-current-shell' to: %s" peut-gerer-current-shell))

(defun peut-gerer-set-command-prefix (prefix)
  "Set `peut-gerer-command-prefix' to PREFIX.

PREFIX may be any string."
  (interactive
   (list (read-string "Set command prefix: " "python" nil "python")))
  (setq peut-gerer-command-prefix prefix)
  (message "Set `peut-gerer-command-prefix' to %s" peut-gerer-command-prefix))

(defun peut-gerer-set-command (new-command)
  "Set `peut-gerer-command' to NEW-COMMAND.

NEW-COMMAND can be any string."
  (interactive "sShell command: ")
  (setq peut-gerer-command new-command))

(defun peut-gerer-buffer-file-to-shell ()
  "Send current buffer file to shell as temporary postfix to
`peut-gerer-command-prefix'."
  (interactive)
  (let ((file (buffer-file-name)))
    (if file
        (peut-gerer-send-command (concat peut-gerer-command-prefix " \"" file "\""))
      (error "Command not sent. Buffer not visiting file"))))

(defun peut-gerer-set-command-to-current-file ()
  "Postfix current buffer file to `peut-gerer-command-prefix'.

This is useful if, for instance, a project was started using one
file, but later in development another file needs to be called
frequently.  It is like a permanent version of
`peut-gerer-buffer-file-to-shell'."
  (interactive)
  (setq peut-gerer-command (concat peut-gerer-command-prefix " "
                                   (format "\"%s\"" (buffer-file-name))))
  (message "Set `peut-gerer-command' to \"%s\"" peut-gerer-command))

(defun peut-gerer-send-region (&optional beg end pbuff)
  "Send region defined by BEG and END to shell process buffer PBUFF.

Use current region if BEG and END not provided.  Default PBUFF is
`peut-gerer-current-shell'."
  (interactive (if (use-region-p)
                   (list (region-beginning) (region-end) nil)
                 (list nil nil nil)))
  (let* ((beg (or beg (if (use-region-p) (region-beginning)) nil))
         (end (or end (if (use-region-p) (region-end)) nil))
         (substr (or (and beg end (buffer-substring-no-properties beg end)) nil))
         (pbuff (or pbuff peut-gerer-current-shell)))
    (if substr
        (peut-gerer-send-command substr pbuff)
      (error "No region selected"))))

(defun peut-gerer-open-dir (dirname &optional type)
  "Open all files with EXTENSION in DIRNAME.

Optional regex TYPE to open.

See URL `https://emacs.stackexchange.com/a/46480/15177'"
  (interactive "DOpen files in: ")
  (let ((type (or type "\\.py$")))
    (mapc #'find-file (directory-files dirname t type nil))))

(defun peut-gerer-open-dir-recursively (dirname &optional ext)
  "Open all files in DIRNAME with EXT extension.  Default EXT is '.py'.

See URL `https://emacs.stackexchange.com/a/46480'"
  (interactive "DRecursively open dir: ")
  (unless ext (setq ext "py"))
  (let ((regexp (concat "\\." ext "$")))
    (mapc #'find-file (directory-files-recursively dirname regexp nil))))

(provide 'peut-gerer)

;;; peut-gerer ends here
