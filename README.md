# peut-gerer
Peut-gerer Enables Users To...manage project workflows.

**Table of Contents**
- [Overview](#overview)
- [Define projects](#define-projects)
  - [Shells](#shells)
  - [Paths](#paths)
  - [Commands](#commands)
  - [Prefix](#prefix)
  - [Window split](#window-split)
  - [Hooks](#hooks)

# Overview
A project is a collection of shell processes, paths, commands, and
editor configurations.  Shells are distinguished by name and each has
its own setup.  Only one project may be active at a time.  Use
separate Emacs instances to activate concurrent projects.

# Define projects
Define projects in `peut-gerer-project-alist`.  Activate projects with
`peut-gerer-activate`.  Deactivate them with `peut-gerer-deactivate`.
Projects are activated using the current value of
`peut-gerer-projects-a list`.

Valid keys:

- `shells`
  - `root`
  - `setup`
- `paths`
- `commands`
- `prefix`
- `window-split`
- `hooks`

For example:

```lisp
(setq peut-gerer-project-alist
      '((my_c_project .
                      ((shells .
                               ((shell-name-1 .
                                              ((root . "/path/to/build/directory/")
                                               (setup . ("export MYBUILDVAR1=1"
                                                         "export MYBUILDVAR2=0"
                                                         ))))
                                (shell-name-2 .
                                              ((root . "/path/to/run/directory/")
                                               (setup . ("export MYRUNVAR=1"))))))
                       (paths . ("/path/to/entrypoint.c"
                                 "/path/to/other/file/I/care/about.c"))
                       (commands . ("./build_my_project.sh"
                                    "./run_my_build"))
                       (window-split . :quad)))
        (my-python-project .
                           ((shells .
                                    ((my-python-project .
                                                        ((root . "/path/to/my/python/project")
                                                         (setup . ("source venv/bin/activate"))))))
                            (paths . ("/path/to/my_python_project/entry_point.py"))
                            (commands . ("python3 -m my_python_project"
                                         "python3 -m unittest discover tests/ --failfast --quiet"))
                            (prefix . "python3 ")
                            (window-split . :vertical)
                            (hooks . ((before-activate . ((lambda () (message "before-activate1"))
                                                          (lambda () (message "before-activate2"))))
                                      (after-activate . ((lambda () (message "after-activate"))))
                                      (before-deactivate . ((lambda () (message "before-deactivate"))))
                                      (after-deactivate . ((lambda () (message "after-deactivate"))))))))))
```

Top level symbols define a project.  Each project may define `shells`,
`commands`, `paths`, a `prefix`, a `window-split`, and `hooks`.

## Shells
The `shells` key specifies shell processes to create.  Shell processes
are
[comints](https://git.savannah.gnu.org/cgit/emacs.git/tree/lisp/comint.el),
such as with `M-x shell`.

The key of the nested shell alist gives the shell name.  For example,
the `build` key creates `*build*` and `run` creates `*run*`.

Each shell has a `root` and a list of `setup` commands.  String
commands are sent to the corresponding shell.

```lisp
((shells .
         ((build .                                           ; "*build*" shell, default shell is given first
                 ((root . "/path/to/build/directory/")
                  (setup . ("export MYBUILDVAR1=1"           ; sent to "*build*" shell
                            "export MYBUILDVAR2=0"))))       ; sent to "*build*" shell
          (run .                                             ; "*run*" shell
               ((root . "/path/to/run/directory/")
                (setup . ("export MYRUNVAR=1")))))))         ; sent to "*run*" shell
```

Only one shell is considered current at a time.  The current shell is
stored in `peut-gerer-current-shell`.  The first shell listed is the
default current shell.

## Paths
The `paths` key corresponds to files to load.

Paths have three forms:

1. File – load specific file.  Use absolute paths for best
   results.
   E.g. `"/load/this/exact/file.c"`
2. Directory – load all files at the directory level. **Directory
   loading is not recursive!**  You must explicitly list all
   directories to load.  E.g. `"/load/files/in/directory/top/level/"`
3. Wildcard – glob directory for type of file.
   E.g. `"/load/only/C/files/*.c"`

```lisp
(paths . ("/home/me/Projects/MyProject/my_project/entry_point.py"  ; loads specific file
          "/home/me/Projects/MyProject/tests/"                     ; loads all files in directory
          "/home/me/Projects/MyProject/my_project/*.c"             ; only loads .c files
          ))
```

Paths are expanded to a list of files, stored in `peut-gerer-files`.

## Commands
The `commands` key corresponds to stored shell commands.

Commands apply to the current shell, `peut-gerer-current-shell`.  Send
commands with `peut-gerer-send-command`.  There exists a primary
command, `peut-gerer-command`, which defaults to the first command
listed.  Subsequent commands are added to the command history.  Press
`<down>` or `C-n` to access these commands when calling
`peut-gerer-send-command` interactively.

```lisp
(commands . ("python3 -m unittest discover tests/ --failfast --quiet"  ; primary command
             "python3 -m my_project"                                   ; loaded into command history
             ))
```

## Prefix
A `prefix` is a string which precedes another.  Typically, this is a
command or a program, such as `python` or `./my-script`.

If no `commands` are defined yet paths and a prefix exists, then
`peut-gerer-command` is set to `<prefix><first-file>`.  For example,
the following alist

```lisp
(setq peut-gerer-project-alist
      '((my-project .
                    ((shells .
                             ((my_project .
                                          ((root . "/home/ahab/Projects/MyProject/")))))
                     (paths . ("/home/ahab/Projects/MyProject/my_project/main_window.py"))
                     (prefix . "python ")))))
```

would create the command

```
python "/home/ahab/Projects/MyProject/my_project/main_window.py"
```

The `prefix` is stored in `peut-gerer-command-prefix` and is used by
commands such as `peut-gerer-buffer-file-to-shell`.

## Window Split
The `window-split` defines how to arrange Emacs's windows.

Default window splits are `:horizontal`, `:vertical`, and `:quad`:

```
:horizontal   +-------------+
              |1            |
              |             |
              +-------------+
              |sh/2         |
              |             |
              +-------------+

:vertical     +------+------+
              |1     |sh/2  |
              |      |      |
              |      |      |
              |      |      |
              |      |      |
              +------+------+

:quad         +------+------+
              |1     |3     |
              |      |      |
              +------+------+
              |2     |sh/4  |
              |      |      |
              +------+------+
```

Files given by the `paths` key are loaded into the windows according
to the order they are listed (i.e. file1 is put in window1, etc.).
See the diagram above.  If a shell is defined for the project, then
the first shell is displayed rather than the file.

Define custom window arrangements using
`peut-gerer-window-split-alist`.  Add a new keyword corresponding to a
function which takes an optional alist.  When called by
`peut-gerer-activate-project`, the `peut-gerer-project-alist` is
passed.  Use `peut-gerer-files`, `peut-gerer-shell-names`,
`peut-gerer-shell-procs`, and `peut-gerer-roots` to get the
corresponding items for the currently loaded project.

```lisp
(add-to-list 'peut-gerer-window-split-alist
             '(:custom .
                       (lambda (&optional args)
                          (let* ((alist (alist-get peut-gerer-project (car args)))
                                 (file1 (if peut-gerer-files (nth 0 peut-gerer-files))))
                        (delete-other-windows)
                        ;; do custom windowing stuff here
                        ))))
```

## Hooks
The `hooks` key provides functions to run before or after activation
and deactivation.

Hook functions take no arguments.  Use `peut-gerer-files`,
`peut-gerer-shell-names`, `peut-gerer-shell-procs`, and
`peut-gerer-roots` to get corresponding items for the currently loaded
project (when available).

There are four hook keywords:

- `before-activate`: List of functions to call before activating a
   project.  No project files or processes are available.
- `after-activate`: List of functions to call after activating a
   project.  All project files and processes are available.
- `before-deactivate`: List of functions to call before deactivating a
   project.  All project files and processes are available.
- `after-deactivate`: List of functions to call after deactivating a
   project.  No project files or processes are available.

Hook functions run in the order they are listed.

```lisp
(hooks . ((before-activate . ((lambda () (message "before-activate1, called first"))
                              (lambda () (message "before-activate2, called second"))))
          (after-activate . ((lambda () (message "after-activate"))))
          (before-deactivate . ((lambda () (message "before-deactivate"))))
          (after-deactivate . ((lambda () (message "after-deactivate"))))))
```
